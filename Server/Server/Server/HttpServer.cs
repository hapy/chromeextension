﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Server
{
    public class HttpServer
    {
        public TcpListener tcpLestener;

        public bool running = false;

        public HttpServer(int port)
        {
            tcpLestener = new TcpListener(IPAddress.Any, port);
        }

        public void Start()
        {
            Thread thread = new Thread(new ThreadStart(Run));
            thread.Start();
        }

        private void Run()
        {
            tcpLestener.Start();
            running = true;

            Console.WriteLine("Server is running");

            while(running)
            {
                Console.WriteLine("Waiting for connection");
                TcpClient tcpClient = tcpLestener.AcceptTcpClient();
                Console.WriteLine("Client connected");
                HandleClient(tcpClient);
                tcpClient.Close();
            }
        }

        private void HandleClient(TcpClient tcpClient)
        {
            StreamReader reader = new StreamReader(tcpClient.GetStream());
            string message = "";

            while(reader.Peek() != -1)
            {
                message += reader.ReadLine() + "\n";
            }

            Console.WriteLine($"REQUEST \r\n {message}");
        }
    }
}
