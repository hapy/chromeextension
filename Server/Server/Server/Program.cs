﻿using System;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpServer server = new HttpServer(8081);
            server.Start();
        }
    }
}
