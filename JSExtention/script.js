function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function demo() {
    console.log('Taking a break...');
    await sleep(2000);
    getPrice();
}

var Good = class {
    constructor(name, price) {
        this.name = name;
        this.price = price;
    }
};

var goods = [];
var oldGoods = [];

chrome.storage.sync.set({ 'goods': oldGoods }, function () {
    console.log("saved");
    console.dir(oldGoods);
});

function getPrice ()
{
    //var e = document.querySelector(XPathExpression("//*[@class='main-goods__tile']"))
    var e = document.getElementsByClassName("main-goods__tile");
    for (var x of e)
    {
        goods.push(new Good(x.childNodes[4].innerText, x.childNodes[6].innerText))
    }

    chrome.storage.sync.get(['goods'], function (result) {
        if (result.goods === undefined) {
            chrome.storage.sync.set({ 'goods': goods }, function () {
                console.log("saved dict first time");
                console.dir(goods);
            });
        }
        else {
            oldGoods = result.goods;
            for (var i of goods) {
                for (var j of oldGoods) {
                    if (i.name === j.name) {

                    }
                    else {
                        oldGoods.push(i);
                    }
                }
            }
            console.log("new list of items");
            console.dir(oldGoods);
        }
    });
    chrome.storage.sync.set({ 'goods': oldGoods }, function ()
    {
        console.log("saved");
        console.dir(oldGoods);
    });
}

window.addEventListener('scroll', function () {
    getPrice();
});

demo();